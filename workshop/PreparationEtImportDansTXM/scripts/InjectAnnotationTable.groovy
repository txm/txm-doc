import org.txm.utils.CsvReader
import java.nio.charset.Charset

def xmlDir = new File("/home/mdecorde/TEMP/annotation") // contains the xml-txm
def outDir = new File("/home/mdecorde/TEMP/annotation-out") // folder which contains results
def csvFile = new File("/home/mdecorde/TEMP/AnnotationTable.tsv") // the annotation table file to use
def properties = ["lemma"] // the property to inject

println "Inject Annotations\n xml files $xmlDir\n save in $outDir\n read from $csvFile\n using $properties"
outDir.deleteDir()
outDir.mkdir()

CsvReader records = new CsvReader(csvFile.getAbsolutePath(), "\t".charAt(0), Charset.forName("UTF-8"))
records.readHeaders();

def header = Arrays.asList(records.getHeaders())
for (def s : properties) if (!header.contains(s)) {
	println "Missing annotation property : $s"
	return;
}
for (def s : ["ref", "id", "keyword"]) if (!header.contains(s)) {
	println "Missing annotation property : $s"
	return;
}

def injections = [:]
while (records.readRecord()) {
	String textid = records.get("ref")
	if (!injections.containsKey(textid)) injections[textid] = [:]
	
	def props = [:]
	for (def s : properties) props[s] = records.get(s)
	injections[textid][records.get("id")] = props
}

for (String textid : injections.keySet()) {
	File currentXMLFile = new File(xmlDir, textid+".xml")
	if (!currentXMLFile.exists()) {
		println "missing XML file : "+currentXMLFile
	}
	
	def builder = new AnnotationInjection(currentXMLFile, injections[textid], properties);
	println "$currentXMLFile  "+injections[textid]+" $properties"
	builder.process(new File(outDir, currentXMLFile.getName()));
}

println "done"