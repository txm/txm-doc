# Run the continnum integration suite, compile the software, build and deploy
# the site.
# Sylvain Loiseau 25 mars 2009

TXM=/home/groups/t/te/textometrie/

cd $TXM
mvn site
ssh sylvainloiseau,textometrie:ljmsc2bh@shell.textometrie.net create
mvn site:deploy
