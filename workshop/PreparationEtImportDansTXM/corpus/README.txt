Extrait des sources du corpus VOEUX
===================================
   Septembre 2014 Serge Heiden
   
Copyright © 2013-2014 Jean-Marc Leblanc & ENS de Lyon
Ces données sont diffusées sous licence CC BY-NC-SA 2.0 FR
https://creativecommons.org/licenses/by-nc-sa/2.0/fr

Cet extrait conçu pour les ateliers "préparation & import
de corpus dans TXM" sont issus du corpus de discours de vœux
présidentiels français de 1959 à 2009, édité par Jean-Marc Leblanc.
Une version binaire du corpus complet est publiée par
le projet Textométrie :
https://sourceforge.net/projects/txm/files/corpora/voeux

Il est composé de trois textes :
- t0015, Pompidou, 1973
- t0022, Giscard, 1980
- t0036, Mitterrand, 1994

Ces 3 textes sont fournis sous trois formats différents :
- le répertoire 'voeux-odt' contient les textes au format LibreOffice *Writer*
   Open Document Text (.odt) - équivalent de Microsoft *Word* (.doc)
- le répertoire 'voeux-txt' contient les textes au format texte brut (.txt) en Unicode
- le répertoire 'voeux-xml' contient les textes au format XML (.xml)
- le répertoire 'voeux-xml-full' contient les textes au format XML (.xml) entièrement encodés

Les métadonnées des textes sont fournies en trois formats différents :
- metadata.csv au format *CSV* (.csv) - ',' comme caractère séparateur de colonnes et champs de texte délimités par "
- metadata.ods au format LibreOffice *Calc* Open Document Spreadsheet (.ods)
- metadata.xls au format  Microsoft Office *Excel* (.xls)
Il faut utiliser le fichier metadata.csv avec TXM.
