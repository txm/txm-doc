package org.txm.macro;
// STANDARD DECLARATIONS

import org.kohsuke.args4j.*
import groovy.transform.Field
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.doc.*;
import org.txm.importer.*;

// BEGINNING OF PARAMETERS
@Field @Option(name="inputDirectory",usage="the directory containing the DOC/ODT/RTF files to convert", widget="Folder", required=true, def="")
File inputDirectory

@Field @Option(name="extension",usage="an example file", widget="String", required=true, def="(odt|doc|rtf|html)")
String extension = "doc"

// Open the parameters input dialog box
if (!ParametersDialog.open(this)) return;

boolean debug = false;

ConvertDocument converter;
def files = []
try {
	converter = new ConvertDocument();
	inputDirectory.eachFileMatch(~/.+\.$extension/) { docFile ->
		String name = docFile.getName()
		name = name.substring(0, name.indexOf("."))
		def txtFile = new File(docFile.getParentFile(), name+".txt")
		converter.autoFile(docFile, txtFile, "txt")
		files << docFile
	}
} catch(Exception e) {
	println "Error while processing directory: "+e;
	if (debug) e.printStackTrace();
}
if (converter != null) converter.stop();

println "Processed directory: $inputDirectory"
println "files: "+files