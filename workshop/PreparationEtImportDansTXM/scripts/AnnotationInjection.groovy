import javax.xml.stream.*

import org.txm.importer.StaxIdentityParser
import org.txm.importer.filters.*
import org.txm.importer.graal.PersonalNamespaceContext

/**
 * The Class AnnotationInjection.
 *
 * @author mdecorde
 *
 * inject annotation from a stand-off file into a xml-tei-txm
 * file
 */

public class AnnotationInjection extends StaxIdentityParser {

	public static String TXMNS = "http://textometrie.org/1.0"

	/** The xml reader factory. */
	private def factory;

	def xmlFile;
	def records;
	def properties;

	/**
	 * Instantiates a new annotation injection.
	 *
	 * @param url the xml-tei-txm file
	 * @param anaurl the stand-off file
	 */
	public AnnotationInjection(File xmlFile, def records, def properties) {
		super(xmlFile.toURI().toURL()); // init reader and writer
		
		this.xmlFile = xmlFile
		this.records = records
		this.properties = properties
		
		try {
			factory = XMLInputFactory.newInstance();
		} catch (XMLStreamException ex) {
			System.out.println(ex);
		} catch (IOException ex) {
			System.out.println("IOException while parsing ");
		}
	}

	String wordId;
	String anaType;
	boolean start = false;
	boolean replaceAnnotation = false;
	protected void processStartElement()
	{
		switch (parser.getLocalName()) {
			case "w":
				for (int i= 0 ; i < parser.getAttributeCount() ; i++ ) {
					if (parser.getAttributeLocalName(i) == "id") {
						wordId = parser.getAttributeValue(i);
						if (records.containsKey(wordId)) {
							start = true;
							println "found word $wordId"
						}
						break;
					}
				}
				break;
			case "ana":
				if (start)
				for (int i= 0 ; i < parser.getAttributeCount() ; i++ ) {
					if (parser.getAttributeLocalName(i) == "type") {
						anaType = parser.getAttributeValue(i);
						anaType = anaType.substring(1)
						replaceAnnotation = properties.contains(anaType)
						if (replaceAnnotation)
						//println " replace annotation $anaType"
						break;
					}
				}
				break;
		}
		super.processStartElement();
	}
	
	@Override
	protected void processCharacters() {
		if (start && replaceAnnotation) return; // don't write annotation chars
		super.processCharacters();
	}

	protected void processEndElement()
	{
		if (start) {
			def data = records.get(wordId) // [p1:value1, p2:value2]
			switch (parser.getLocalName()) {
				case "w":
					start = false
					for (def prop : data.keySet()) {
						// write prop	
						writer.writeStartElement(TXMNS, "ana")
						writer.writeAttribute("resp", "txm")
						writer.writeAttribute("type", "#$prop")
						writer.writeCharacters(data.get(prop))
						writer.writeEndElement()
						writer.writeCharacters("\n")
						//println " create annotation $prop"
					}
					break;
				case "ana":
					if (replaceAnnotation) {
						writer.writeCharacters(data.get(anaType))
						data.remove(anaType)
						//println " replace annotation $anaType content"
					}
					anaType = ""
					replaceAnnotation = false;
					break;
			}
		}
		super.processEndElement();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		File xmlFile = new File("/home/mdecorde/TEMP/annotation/01_DeGaulle.xml")
		def properties = ["p1", "lemma"]
		def records = [
			"w_1":["p1":"a", "lemma":"b"],
			"w_224":["p1":"c", "lemma":"d"],
			"w_400":["p1":"e", "lemma":"f"],
			"w_750":["p1":"g", "lemma":"h"],
			"w_753":["p1":"i", "lemma":"j"],
			"w_756":["p1":"k", "lemma":"l"]
			]
		
		def builder = new AnnotationInjection(xmlFile, records, properties);
		builder.process(new File("/home/mdecorde/TEMP", "rez.xml"));

		return;
	}

}